var rpc = require("discord-rpc");
const client = new rpc.Client({ transport: "ipc" });
client.on("ready", () => {
  console.log("Ready, setting status.");
  client.request("SET_ACTIVITY", {
    pid: process.pid,
    activity: {
      details: "Invite Today!",
      state: "https://tritanbot.xyz",
      buttons: [{ label: "Dashboard", url: "https://tritanbot.xyz" }, { label: "Invite", url: "https://tritanbot.xyz/invite" }],
      assets: {
        large_image: "icon",
        small_image: "alertd",
        small_text: "Visit Our Dashboard!",
      },
    },
  });
});
client.login({ clientId: "732783297872003114" }).catch(console.error);

